﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Data;

namespace Calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
           
          
        }

        private void Number_Clicked(object sender, EventArgs e)
        {
            if (label1.Text.Equals("0"))
            {
                label1.Text = " ";
             }

            label1.Text = label1.Text + ((Button)sender).Text;
        }

        private void Del_Clicked(object sender, EventArgs e)
        {
            label1.Text = "0";
            label2.Text = "";

        }

        private void Result_Clicked(object sender, EventArgs e)
        {
            label2.Text = label2.Text + label1.Text;
            App.Current.Resources["bgColor"] = Color.Purple;
            App.Current.Resources["basic"] = Color.Brown;


            Re();
        }

        private void  Calcu_Clicked (object sender, EventArgs e)
        {
            label2.Text = label2.Text + label1.Text + ((Button)sender).Text;

            App.Current.Resources["basic"] = Color.Yellow;

            label1.Text = "0";
            
        }

        void Re()
        {
            DataTable dt = new DataTable();
            string v = dt.Compute(label2.Text, "").ToString();

            label1.Text = v;


        }


    }
}
